
class class_room {
    constructor(room_no, floor, program, available) {
        this.room_no = room_no;
        this.floor = floor;
        this.program = program;
        this.available=available;
    }

    get_room_no() {
        return this.room_no;
    }
    get_floor() {
        return this.floor;
    }
    get_program() {
        return this.program;
    }
    get_available() {
        return this.available;
    }
    set_room_no(value){
        this.room_no=value;
    }
}

const class1 = new class_room(404, 4, "csit",true);
console.log(class1.get_room_no()); // Use class1 to access the property and call the method
console.log(class1.get_available()); // Use class1 to access the property and call the method
class1.set_room_no(504);
console.log(class1.get_room_no());