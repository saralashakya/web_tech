// Standalone JavaScript file (e.g., script.js)

// Accessing the window object
console.log(window);

// Using properties of the window object
console.log(window.innerWidth); // Get the inner width of the browser window
console.log(window.location.href); // Get the current URL
