class Circle {
    constructor(radius) {
      this.shapeType = 'circle';
      this.radius = radius;
    }
    calculate_area_circle(){
        return Math.PI*this.r*this.r;
    }
  }
  class Rectangle {
    constructor(length, breadth) {
      this.shapeType = 'rectangle';
      this.length = length;
      this.breadth = breadth;

    }
    calculate_area_rectangle(){
        return this.length*this.breadth;
    }
  }
  class Triangle {
    constructor(base, height) {
      this.shapeType = 'triangle';
      this.base = base;
      this.height = height;
    }

    calculate_area_triangle(){
        return this.base*this.height;
    }
  }




  const t= new Triangle(2,4);

  if(t instanceof(Triangle)){
    console.log(t.calculate_area_triangle());
  }
  else if(t instanceof(Circle)){
    console.log(t.calculate_area_circle());
  }
  else{
    console.log(t.calculate_area_rectangle());
  }