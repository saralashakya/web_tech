// Question Mark (?) Example

// const pattern1 = /foo?/;
// console.log(pattern1.test("food"));  // true (matches because 'o' appears once)
// console.log(pattern1.test("fod"));   // true (matches because 'o' appears zero times)
// console.log(pattern1.test("faod"));  
// console.log(pattern1.test("fooas"));  


// Plus Sign (+) Example
// const pattern2 = /fo+/;
// console.log(pattern2.test("fod"));    // true (matches because 'o' appears once)
// console.log(pattern2.test("food"));   // true (matches because 'o' appears twice)
// console.log(pattern2.test("foood"));  // true (matches because 'o' appears three times)
// console.log(pattern2.test("fd"));     // false (does not match because 'o' appears zero times)

// Asterisk (*) Example
// const pattern3 = /fo*d/;
// console.log(pattern3.test("fd"));      // true (matches because 'o' appears zero times)
// console.log(pattern3.test("fod"));     // true (matches because 'o' appears once)
// console.log(pattern3.test("food"));    // true (matches because 'o' appears twice)
// console.log(pattern3.test("foood"));   // true (matches because 'o' appears three times)


const pattern4 = /fo{2,}d/;
console.log(pattern4.test("fod"));
