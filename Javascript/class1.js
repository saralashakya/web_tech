class Car {
    constructor(name, year) {
      this.name = name;
      this.year = year;
    }
    age() {
      const date = new Date();
      return date.getFullYear() - this.year;
    }

    get_name(){
      return this.name;  
    }

    set_name(name){
        this.name=name;
    }
  }
  
  const myCar = new Car("Ford", 2014);
  console.log(myCar.age());