<xsl:stylesheet version="1.0" 
xmlns:xsl="http://www.w3.org/2001/XMLSchema-instance">
<xsl:template match="/class">

    <html>
        <body>
            <h2>Student List</h2>
            <table border ="1">
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Nick Name</th>
                </tr>
                <xsl:for-each select="student">
                    <tr>
                        <td><xsl:value-of select="first_name"/></td>
                    </tr>

                </xsl:for-each>
            </table>
        </body>


    </html>



</xsl:template>

</xsl:stylesheet>