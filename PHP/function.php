<?php
function writeMsg() {
  echo "Hello world!";
}
function familyName($fname) {
    echo "$fname Shreshta <br>";
  }
//return type with argument
function familyNames($fname, $year) {
    echo "$fname Shrestha Born in $year <br>";
  }
  writeMsg(); // call the function

function age_difference(int $a, int $b) {
    return $a - $b;
}

familyName("Mina");
familyName("Munu");
$mina_age=1975;
$munu_age=1978;
  
familyNames("Mina",$mina_age);
familyNames("Munu",$munu_age);


echo age_difference($mina_age,$munu_age); 

?>