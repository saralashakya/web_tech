<?php
// Example of a three-dimensional array
$students = array(
    array(
        array("John", 25),
        array("Alice", 22),
    ),
    array(
        array("Bob", 28),
        array("Eva", 21),
    ),
);

// Accessing elements in the three-dimensional array
echo "Age of John: " . $students[0][0][1] . "<br>";
echo "Name of Eva: " . $students[1][1][0] . "<br>";
?>