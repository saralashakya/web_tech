<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "PHP_Practice";

// Create connection
//If the connection is successful, it returns a MySQLi object, which is a representation of the MySQL connection
//If the connection fails for any reason, it returns false
$conn = mysqli_connect($servername, $username, $password, $dbname);
// Check connection
if (!$conn) {
  die("Connection failed: " . mysqli_connect_error());
}

else{
    print("Connected to the database successfully");
}
mysqli_close($conn);
?>