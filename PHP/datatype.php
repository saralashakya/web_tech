<?php
//string type
$x = "Hello world!";
$y = 'Hello world!';
//integer type
$z = 5985;
//float type
$a = 10.365;
//boolean type
$b = true;
$cars = array("Volvo","BMW","Toyota");

echo "The value of x is ".$x;
echo "<br>";
echo "The value of y is ".$y;
echo "<br>";
echo "The value of z is ".$z;
echo "<br>";
echo "The value and type of a is ";
var_dump($a);
echo "<br>";
echo "The value and type of b is ";
var_dump($b);
echo "<br>";
echo"The brand of cars are ";
foreach ($cars as $car) {
    echo $car . " ";
}
?>